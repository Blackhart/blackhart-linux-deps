#--------------------------------------------------------------------------------------------------
# Configure.
#--------------------------------------------------------------------------------------------------

CURRENT_PATH=`pwd`

# Remove the old build if exists
rm -rf build
mkdir build

echo ""

#--------------------------------------------------------------------------------------------------
# Build dependencies.
#--------------------------------------------------------------------------------------------------

echo ""
echo "-----------------"
echo "   Build glfw    "
echo "-----------------"
echo ""

cd src/glfw-3.2.1

# Build the project
cmake \
	-DGLFW_BUILD_DOCS=false \
	-DGLFW_BUILD_EXAMPLES=false \
	-DGLFW_BUILD_TESTS=false \
	-DCMAKE_BUILD_TYPE=Debug \
	-DCMAKE_INSTALL_PREFIX:PATH=$CURRENT_PATH/glfw \
	.

# Compile the project
make install

# Build the project
cmake \
	-DGLFW_BUILD_DOCS=false \
	-DGLFW_BUILD_EXAMPLES=false \
	-DGLFW_BUILD_TESTS=false \
	-DCMAKE_BUILD_TYPE=Release \
	-DCMAKE_INSTALL_PREFIX:PATH=$CURRENT_PATH/glfw \
	.

# Compile the project
make install

cd $CURRENT_PATH

echo ""
echo "-------------------"
echo "   Build cmocka    "
echo "-------------------"
echo ""

cd src/cmocka-1.1.5

mkdir build
cd build

# Build the project
cmake \
	-DBUILD_TESTING=false \
	-DWITH_EXAMPLES=false \
	-DWITH_CMOCKERY_SUPPORT=true \
	-DWITH_STATIC_LIB=true \
	-DCMAKE_BUILD_TYPE=Release \
	-DCMAKE_INSTALL_PREFIX:PATH=$CURRENT_PATH/cmocka-1.1.5 \
	..

# Compile the project
make install

cd $CURRENT_PATH

echo ""
echo "------------------"
echo "   Build vulkan    "
echo "------------------"
echo ""

# Download vulkan 1.1.92
wget -qO - http://packages.lunarg.com/lunarg-signing-key-pub.asc | apt-key add -
wget -qO /etc/apt/sources.list.d/lunarg-vulkan-1.1.92-bionic.list http://packages.lunarg.com/vulkan/1.1.92/lunarg-vulkan-1.1.92-bionic.list
apt update
apt install -y lunarg-vulkan-sdk